"""
Simple tool for checking a bunch of Nextcloud servers, whether their Nextcloud version is supported and up2date.

 Author: wh0ami
License: MIT License <https://opensource.org/license/MIT>
Project: https://codeberg.org/wh0ami/NextcloudVersionChecker
"""

import argparse
import sys


def cli_argument_parser() -> argparse.Namespace:
    """
    Process passed arguments from CLI.

    :return: Namespace with all passed parameters.
    """
    parser = argparse.ArgumentParser(
        description="Simple tool for checking a bunch of nextcloud servers, whether their Nextcloud version is "
        "supported and up2date.",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument(
        "-s",
        "--server-list",
        metavar="<JSON file with servers>",
        default="input.json",
        help="Path of a JSON file with a simple list of URLs of Nextcloud servers. Defaults to './input.json'.",
    )
    return parser.parse_args(sys.argv[1:])
