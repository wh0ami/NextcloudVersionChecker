"""
Simple tool for checking a bunch of Nextcloud servers, whether their Nextcloud version is supported and up2date.

 Author: wh0ami
License: MIT License <https://opensource.org/license/MIT>
# Project: https://codeberg.org/wh0ami/NextcloudVersionChecker
"""

import json
import sys
from pathlib import Path

from loguru import logger


def validate_input_data(input_data: list[str]) -> None:
    """
    Validate the types and values of the passed input data.

    :param input_data:
    """
    if not isinstance(input_data, list):
        raise TypeError("JSON structure is not a list")

    if len(input_data) <= 0:
        raise ValueError("JSON list is empty")

    for item in input_data:
        if not isinstance(item, str):
            raise TypeError(
                f"JSON list contains an value, which is not a string: '{item}' of type '{type(item).__name__}'",
            )

        if len(item) == 0:
            raise ValueError("JSON list contains an empty string")

        if not item.startswith("http://") and not item.startswith("https://"):
            raise ValueError(f"JSON list contains an URL, which is not starting with 'http://' or 'https://': '{item}'")

        if item.endswith("/"):
            raise ValueError(f"JSON list contains an URL, which ends with a '/' (but must not): '{item}'")

        if " " in item:
            raise ValueError(f"JSON list contains an URL, which contains a whitespace (but must not): '{item}'")

        if "\t" in item:
            raise ValueError(f"JSON list contains an URL, which contains a tabulator (but must not): '{item}'")


def input_data_loader(input_file_path: str) -> list[str]:
    """
    Load the input data from a toml file.

    :param input_file_path: Path of the input file, which should be loaded.
    :return: List of servers from the configuration file.
    """
    input_file = Path(input_file_path)

    try:
        with input_file.open("r") as input_file_handler:
            input_data = json.load(input_file_handler)
            validate_input_data(input_data=input_data)
    except FileNotFoundError:
        logger.error(f"The specified input file '{input_file.absolute()}' doesn't exist.")
        sys.exit(1)
    except PermissionError:
        logger.error(f"The specified input file '{input_file.absolute()}' is not accessible.")
        sys.exit(1)
    except json.JSONDecodeError:
        logger.error(f"The specified input file '{input_file.absolute()}' doesn't contain valid JSON.")
        sys.exit(1)
    except TypeError as exception:
        logger.error(f"The specified input file '{input_file.absolute()}' contains invalid data types:")
        logger.error(f"-> {exception}")
        sys.exit(1)
    except ValueError as exception:
        logger.error(f"The specified input file '{input_file.absolute()}' contains invalid data values:")
        logger.error(f"-> {exception}")
        sys.exit(1)

    return input_data
