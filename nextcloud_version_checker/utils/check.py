"""
Simple tool for checking a bunch of Nextcloud servers, whether their Nextcloud version is supported and up2date.

 Author: wh0ami
License: MIT License <https://opensource.org/license/MIT>
Project: https://codeberg.org/wh0ami/NextcloudVersionChecker
"""

import urllib.error
import xml

from loguru import logger

from nextcloud_version_checker.nextcloud.instance import fetch_current_nextcloud_status
from nextcloud_version_checker.nextcloud.updater_server import fetch_version_information
from nextcloud_version_checker.nextcloud.versioning import Channel, semver_to_nextcloud_version


def red(string: str) -> str:
    """Color a string bold red."""
    return f"\033[1m\033[91m{string}\033[0m"


def green(string: str) -> str:
    """Color a string bold green."""
    return f"\033[1m\033[92m{string}\033[0m"


def yellow(string: str) -> str:
    """Color a string bold orange."""
    return f"\033[1m\033[93m{string}\033[0m"


def cyan(string: str) -> str:
    """Color a string bold cyan."""
    return f"\033[1m\033[36m{string}\033[0m"


def check(instance_url: str) -> list[str]:
    """
    Check the current update status of a Nextcloud instance.

    :param instance_url: URL of the Nextcloud instance to check.
    :return: Tabulate compatible list with data about the current status of the Nextcloud instance.
    """
    logger.info(f"Checking '{instance_url}'...")
    try:
        instance_status = fetch_current_nextcloud_status(url=instance_url)
    except urllib.error.URLError as exception:
        logger.error(f"Error while fetching the current version of '{instance_url}':")
        logger.error(f"-> {exception}")
        instance_status = None

    if instance_status:
        current_version = instance_status["version"]
        release_channel = instance_status["channel"]
        extended_support = instance_status["extended_support"]
        current_version_string = semver_to_nextcloud_version(version=current_version)
        match release_channel:
            case Channel.ENTERPRISE:
                release_channel_string = cyan(release_channel.value.capitalize())
            case Channel.BETA:
                release_channel_string = yellow(release_channel.value.capitalize())
            case Channel.STABLE:
                release_channel_string = release_channel.value.capitalize()
            case _:
                raise RuntimeError(f"Got invalid release channel '{release_channel}'!")
        extended_support_string = green("Yes") if extended_support else "No"

        try:
            version_information = fetch_version_information(
                current_version=current_version,
                release_channel=release_channel,
                extended_support=extended_support,
            )
        except urllib.error.URLError as exception:
            logger.error(f"Error while fetching the version information from the updater server for '{instance_url}':")
            logger.error(f"-> {exception}")
            version_information = None
        except xml.parsers.expat.ExpatError as exception:
            logger.error(
                f"Error while parsing the fetched version information from the updater server for '{instance_url}':",
            )
            logger.error(f"-> {exception}")
            version_information = None

        if version_information:
            supported = red("✖") if version_information["is_eol"] else green("✓")
            latest = red("✖") if version_information["latest_version"] > current_version else green("✓")
            available_version = (
                yellow(
                    semver_to_nextcloud_version(
                        version=version_information["latest_version"],
                    ),
                )
                if version_information["latest_version"] > current_version
                else "-"
            )
        else:
            # on error while fetching the version information from the updater server
            supported = red("Error")
            latest = red("Error")
            available_version = red("Error")
    else:
        # on error while fetching the current status from the nextcloud instance
        current_version_string = red("Error")
        release_channel_string = "?"
        extended_support_string = "?"
        supported = "?"
        latest = "?"
        available_version = "?"

    return [
        instance_url.split("://")[1],
        current_version_string,
        release_channel_string,
        extended_support_string,
        supported,
        latest,
        available_version,
    ]
