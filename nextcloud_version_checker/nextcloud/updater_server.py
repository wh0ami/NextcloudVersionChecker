"""
Simple tool for checking a bunch of Nextcloud servers, whether their Nextcloud version is supported and up2date.

Uses local caching to avoid too much requests to the server

 Author: wh0ami
License: MIT License <https://opensource.org/license/MIT>
Project: https://codeberg.org/wh0ami/NextcloudVersionChecker
"""

import json
import urllib.request
import urllib.response
from datetime import UTC, datetime

import semver
import xmltodict

from nextcloud_version_checker.nextcloud.versioning import (
    Channel,
    nextcloud_version_to_semver,
)

version_information_cache: dict[str, bytes] = {}


def fetch_version_information(
    current_version: semver.Version,
    release_channel: Channel,
    extended_support: bool,
) -> dict:
    """
    Request information for a specific version from the updater server.

    Upstream code:
    - https://github.com/nextcloud/server/blob/master/lib/private/Updater/VersionCheck.php
    - https://github.com/nextcloud/server/blob/master/lib/public/Util.php#L40
    - https://github.com/nextcloud/server/blob/master/lib/public/ServerVersion.php
    - https://github.com/nextcloud/server/blob/master/version.php
    - https://www.php.net/manual/en/reserved.constants.php

    :param current_version: Current version of the Nextcloud instance to ask for.
    :param release_channel: Release channel, that should be used.
    :param extended_support: Whether the Nextcloud has extended support / a subscription.
    :return: Dict of information
    """
    cache_key = json.dumps(
        {
            "version": str(current_version),
            "release_channel": release_channel.value,
            "extended_support": extended_support,
        },
    )

    if cache_key not in version_information_cache:
        fictional_timestamp_installation = str(datetime.now(tz=UTC).timestamp() - (30 * 24 * 60 * 60))
        fictional_timestamp_update = str(datetime.now(tz=UTC).timestamp() - (14 * 24 * 60 * 60))
        api_parameters = "x".join(
            [
                str(current_version.major),  # Current major version of Nextcloud
                str(current_version.minor),  # Current minor version of Nextcloud
                str(current_version.patch),  # Current patch version of Nextcloud
                str(current_version.build),  # Current build version of Nextcloud
                fictional_timestamp_installation,  # Timestamp of the installation
                fictional_timestamp_update,  # Timestamp of the last update
                release_channel.value,  # Release channel, that is configured for the Nextcloud
                "",  # Edition, seems to be unused (hardcoded empty string in upstream code)
                "",  # The build timestamp and hash of the Nextcloud, optional
                "999",  # PHP Major version (we want all updates, independent of any PHP version)
                "999",  # PHP Minor version (we want all updates, independent of any PHP version)
                "999",  # PHP Release version (we want all updates, independent of any PHP version)
                "0",  # Compute Category, basically the importance of an instance, we just pass zero
                "1" if extended_support else "0",  # Subscription status of the Nextcloud instance
            ],
        )
        http_request = urllib.request.Request(
            method="GET",
            url=f"https://updates.nextcloud.com/updater_server/?version={api_parameters}",
        )

        with urllib.request.urlopen(url=http_request, timeout=10) as http_response:  # noqa: S310
            version_information_cache[cache_key] = http_response.read()

    if len(version_information_cache[cache_key]) <= 0:
        # in this case, no update is available and the instance is up-to-date
        eol = False
        latest_version = current_version
    else:
        version_information = xmltodict.parse(
            xml_input=version_information_cache[cache_key].decode("UTF-8"),
        )["nextcloud"]
        eol = bool(version_information["eol"] == "1")
        latest_version = nextcloud_version_to_semver(version_string=version_information["version"])

    return {
        "is_eol": eol,
        "latest_version": latest_version,
    }
