"""
Simple tool for checking a bunch of Nextcloud servers, whether their Nextcloud version is supported and up2date.

 Author: wh0ami
License: MIT License <https://opensource.org/license/MIT>
Project: https://codeberg.org/wh0ami/NextcloudVersionChecker
"""

import json
import urllib.request
import urllib.response

import semver

from nextcloud_version_checker.nextcloud.versioning import Channel, nextcloud_version_to_semver


def fetch_current_nextcloud_status(url: str) -> dict[str, Channel | semver.Version | bool]:
    """
    Fetch the current status of a Nextcloud instance.

    :param url: URL of the Nextcloud, whose version should be fetched.
    :return: Requested Nextcloud version as semver object.
    """
    status_endpoint_url = f"{url}/status.php"

    http_request = urllib.request.Request(  # noqa: S310
        method="GET",
        url=status_endpoint_url,
        headers={"User-Agent": "Nextcloud-Version-Checker/1.0"},
    )

    with urllib.request.urlopen(url=http_request, timeout=10) as http_response:  # noqa: S310
        status = json.loads(http_response.read().decode("utf-8"))

    lower_version_string = status["versionstring"].lower()
    if "enterprise" in lower_version_string:
        channel = Channel.ENTERPRISE
    elif "beta" in lower_version_string or "alpha" in lower_version_string or "rc" in lower_version_string:
        channel = Channel.BETA
    else:
        channel = Channel.STABLE

    # legacy fix
    if "extendedSupport" not in status:
        status["extendedSupport"] = "0"

    return {
        "version": nextcloud_version_to_semver(version_string=status["version"]),
        "channel": channel,
        "extended_support": bool(status["extendedSupport"] == 1),
    }
