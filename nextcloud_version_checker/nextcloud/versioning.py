"""
Simple tool for checking a bunch of Nextcloud servers, whether their Nextcloud version is supported and up2date.

 Author: wh0ami
License: MIT License <https://opensource.org/license/MIT>
Project: https://codeberg.org/wh0ami/NextcloudVersionChecker
"""

from enum import Enum

import semver


class Channel(Enum):
    """Release channel of Nextcloud."""

    ENTERPRISE = "enterprise"
    BETA = "beta"
    STABLE = "stable"


def nextcloud_version_to_semver(version_string: str) -> semver.Version:
    """
    Convert a semver-incompatible nextcloud version string into a semver object.

    :param version_string: Nextcloud version string to convert.
    :return: SemVer-object of the passed version string.
    """
    version_parts = [int(part) for part in version_string.split(".")]

    return semver.Version(
        major=version_parts[0],
        minor=version_parts[1],
        patch=version_parts[2],
        build=version_parts[3] if len(version_parts) >= 4 else None,
    )


def semver_to_nextcloud_version(version: semver.Version) -> str:
    """
    Convert a semver object into a semver-incompatible nextcloud version string.

    :param version: SemVer-object to convert
    :return: Nextcloud version string of the passed semver-object.
    """
    version_string = f"{version.major}.{version.minor}.{version.patch}"
    if version.build:
        version_string += f".{version.build}"
    return version_string
