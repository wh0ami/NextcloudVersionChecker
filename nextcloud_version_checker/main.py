"""
Simple tool for checking a bunch of Nextcloud servers, whether their Nextcloud version is supported and up2date.

 Author: wh0ami
License: MIT License <https://opensource.org/license/MIT>
Project: https://codeberg.org/wh0ami/NextcloudVersionChecker
"""

import multiprocessing
import sys

import loguru
from tabulate import tabulate

from nextcloud_version_checker.utils.check import check
from nextcloud_version_checker.utils.cli import cli_argument_parser
from nextcloud_version_checker.utils.input import input_data_loader


def main() -> None:
    """argparser, config loading, etc."""
    cli_arguments = cli_argument_parser()

    input_data = input_data_loader(input_file_path=cli_arguments.server_list)

    pool = multiprocessing.Pool(multiprocessing.cpu_count())
    table = pool.map(check, input_data)

    # generate and print table with results
    loguru.logger.info(
        "+++ Nextcloud Version Overview +++\n"
        + tabulate(
            table,
            headers=[
                "Instance",
                "Release",
                "Type",
                "Extended Support",
                "Supported",
                "Up-to-date",
                "Available release",
            ],
        ),
    )

    # regular script end
    sys.exit(0)
