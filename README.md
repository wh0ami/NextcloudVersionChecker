# Nextcloud Version Checker

This is a tiny and simple tool for checking a bunch of nextcloud servers, 
whether their Nextcloud version is supported and up2date.

## Usage

### Installation

This project is published on
[PyPI](https://pypi.org/project/nextcloud-version-checker/), so you just want
to install it via pipx or pip. Please note, that pipx is the recommended way.

```commandline
$ pipx install nextcloud-version-checker
 
OR

$ pip install nextcloud-version-checker
```

### How to use

You can call the tool via `nvc` or `nextcloud-version-checker`.

It assumes, that there's an existing input file called `input.json` in your
current directory. If you want to specify a custom input file, just call it
like e.g. `nextcloud-version-checker --server-file myservers.json` or shorter
`nvc -s myservers.json`.

An example input file is located under
`nextcloud_version_checker/misc/input.example.json`.

## How it works

Information about the current latest versions will be fetched from the official
API, which is also fetched by the built-in version check.
([Code Reference on ShitHub](https://github.com/nextcloud/server/blob/master/lib/private/Updater/VersionCheck.php))

The script will throw the list of strings from your input file into a
multiprocessing pool which will fetch, parse and display the update information
in a table at the end.

## Example output

```
user@pc:~/Workspace nextcloud-version-checker
2025-02-09 13:58:39.375 | INFO     | nextcloud_version_checker.utils.check:check:46 - Checking 'https://stable.example.org'...
2025-02-09 13:58:39.375 | INFO     | nextcloud_version_checker.utils.check:check:46 - Checking 'https://nightly.example.org'...
2025-02-09 13:58:39.375 | INFO     | nextcloud_version_checker.utils.check:check:46 - Checking 'https://enterprise.example.org'...
2025-02-09 13:58:39.375 | INFO     | nextcloud_version_checker.utils.check:check:46 - Checking 'https://eol.example.org'...
2025-02-09 13:58:39.570 | INFO     | nextcloud_version_checker.utils.check:check:46 - Checking 'https://outdated.example.org'...
2025-02-09 13:58:39.707 | INFO     | nextcloud_version_checker.utils.check:check:46 - Checking 'https://subpath.example.org/nextcloud'...
2025-02-09 13:58:39.754 | INFO     | nextcloud_version_checker.utils.check:check:46 - Checking 'https://offline.example.org'...
2025-02-09 13:58:39.754 | ERROR    | nextcloud_version_checker.utils.check:check:50 - Error while fetching the current version of 'https://offline.example.org':
2025-02-09 13:58:39.754 | ERROR    | nextcloud_version_checker.utils.check:check:51 - -> <urlopen error [Errno 111] Connection refused>
2025-02-09 13:58:40.609 | INFO     | nextcloud_version_checker.main:main:30 - +++ Nextcloud Version Overview +++
Instance                                     Release    Type        Extended Support    Supported    Up-to-date    Available release
-------------------------------------------  ---------  ----------  ------------------  -----------  ------------  -------------------
stable.example.org                           30.0.5.1   Stable      No                  ✓            ✓             -
nightly.example.org                          30.0.6.0   Beta        No                  ✓            ✖             31.0.0.14
enterprise.example.org                       29.0.11.3  Enterprise  No                  ✓            ✓             -
eol.example.org                              15.0.0.10  Stable      No                  ✖            ✖             15.0.14.1
outdated.example.org                         30.0.0.14  Stable      No                  ✓            ✖             30.0.5.1
subpath.example.org/nextcloud                30.0.5.1   Stable      No                  ✓            ✓             -
offline.example.org                          Error      ?           ?                   ?            ?             ?

user@pc:~/Workspace$ 
```

## Development

This is an uv project. You can set up your development environment by cloning
this Repository via `git` and running a `uv sync` in the project
directory afterward.

The Lockfile can be updated by using `uv lock` (also updates the installed
packages in the dependency tree).

The project can be bundled by running `uv build` and published by running
`uv publish`. Results can be found in the `dist/` directory.

For code linting and formatting, `ruff` was used. You may run it via
`uv run ruff check` or `uv run ruff format`.

This project is using [Semantic Versioning](https://semver.org/) and
[Conventional Commits](https://www.conventionalcommits.org/en/).
